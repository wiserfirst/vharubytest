require 'bowling/game'

module Bowling

  describe "parsing input" do
    it "should return a list of integers for valid input" do
      expect(Game.parse("10 2 3 4 5")).to eq [10, 2, 3, 4, 5]
    end

    it "should return an empty list for invalid input" do
      expect(Game.parse("1 integer 4")).to eq []
    end
  end

  describe "handle one frame" do
    it "should work for basic cases" do
      expect(Game.handle_frame([6, 0, 3, 4, 5, 6], 10)).to eq [[3,4,5,6], 16]
    end

    it "should work for the strike" do
      expect(Game.handle_frame([10, 5, 4, 9, 1], 0)).to eq [[5,4,9,1], 19]
    end

    it "should work for the spare" do
      expect(Game.handle_frame([8, 2, 4, 3], 14)).to eq [[4,3], 28]
    end
  end

  describe "get total score" do
    it "should return score for basic cases" do
      expect(Game.get_total_score([1, 2, 3, 4, 5, 6])).to eq 21
    end

    it "should return score for the strike" do
      expect(Game.get_total_score([1, 5, 10, 3, 4, 5])).to eq 35
    end

    it "should return score for the spare" do
      expect(Game.get_total_score([4, 3, 8, 2, 5, 4])).to eq 31
    end

    it "should return score for the perfect game" do
      scores = [10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10]
      expect(Game.get_total_score(scores)).to eq 300
    end
  end

end
