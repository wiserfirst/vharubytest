module Bowling

  class Game
    def Game.parse(input)
      results = input.split(' ')
      if results.all? { |x| x =~ /^(\d|10)$/ }
        results.map(&:to_i)
      else
        []
      end
    end

    def Game.handle_frame(scores, total)
      a, b, c = scores
      b = 0 if b.nil?
      c = 0 if c.nil?

      if a == 10 # the strike
        rolls_in_frame = 1
        current = a + b + c
      else
        rolls_in_frame = 2
        if a + b == 10 # the spare
          current = a + b + c
        else
          current = a + b
        end
      end
      [scores.drop(rolls_in_frame), total + current]
    end

    def Game.get_total_score(scores)
      total = 0
      count = 0
      while !scores.empty? && count < 10
        scores, total = Game.handle_frame(scores, total)
        count += 1
      end
      total
    end
  end
end
