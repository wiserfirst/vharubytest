#Vodafone Ruby Tech Test

## Ten Pin Bowling Score
Write a ruby application that takes a string of space separated numbers from 0 to 10
and calculates what that would be as a score in ten pin bowling (hints on how to score in bowling are here: http://bowling2u.com/trivia/game/scoring.asp).

If a final score cannot be determined from the input then method should return the
"current" score (i.e. assumes any remaining bowls score 0).

Example inputs and outputs

* "1 2 3 4" -> 10
* "9 1 9 1" -> 29
* "1 1 1 1 10 1 1" -> 18
* "10 10 10 10 10 10 10 10 10 10 10 10" -> 300

## Usage
The program expects to receive the scores of a bowling game as a string of space separated numbers from standard input and send the total score to standard output. For example:
```
echo "1 2 3 4" | lib/bowling_score # output 10
echo "9 1 9 1" | lib/bowling_score # output 29
echo "1 1 1 1 10 1 1" | lib/bowling_score # output 18
echo "10 10 10 10 10 10 10 10 10 10 10 10" | lib/bowling_score # output 300
```

## Testing
First install dependencies with bundle under project root directory:
```
bundle install
```
Run the tests by using rake:
```
rake
```
